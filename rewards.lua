-- a table of possible hard clue rewards

local THIRD_AGE_TABLE = {
    {"third age full helm",    1, "3ahelm.png"},
    {"third age platebody",    1, "3abody.png"},
    {"third age platelegs",    1, "3alegs.png"},
    {"third age kiteshield",   1, "3ashield.png"},
    {"third age coif",         1, "3adcoif.png"},
    {"third age range body",   1, "3adbody.png"},
    {"third age range legs",   1, "3adlegs.png"},
    {"third age vambraces",    1, "3advamb.png"},
    {"third age robe hat",     1, "3amhat.png"},
    {"third age robe top",     1, "3amtop.png"},
    {"third age robe bottoms", 1, "3amrobe.png"},
    {"third age amulet",       1, "3amamulet.png"},
}

local SUPER_RARE_TABLE = {
    {"gilded full helm",  1, "goldhelm.png"},
    {"gilded platebody",  1, "goldbody.png"},
    {"gilded platelegs",  1, "goldlegs.png"},
    {"gilded plateskirt", 1, "goldskirt.png"},
    {"gilded kiteshield", 1, "goldshield.png"},
    {"super set",         5, "superset.png"},
    {"super restore",     5, "superrestore.png"},
    {"super energy",      5, "superenergy.png"},
    {"antifire",          5, "antifire.png"},
    {THIRD_AGE_TABLE,     1},
}

RARE_REWARDS = {
    {"rune full helm (h1)",   1, "runehelmh1.png"},
    {"rune full helm (h2)",   1, "runehelmh2.png"},
    {"rune full helm (h3)",   1, "runehelmh3.png"},
    {"rune full helm (h4)",   1, "runehelmh4.png"},
    {"rune full helm (h5)",   1, "runehelmh5.png"},
    {"rune kiteshield (h1)",  1, "runeshieldh1.png"},
    {"rune kiteshield (h2)",  1, "runeshieldh2.png"},
    {"rune kiteshield (h3)",  1, "runeshieldh3.png"},
    {"rune kiteshield (h4)",  1, "runeshieldh4.png"},
    {"rune kiteshield (h5)",  1, "runeshieldh5.png"},
    {"rune full helm (t)",    1, "runehelmt.png"},
    {"rune full helm (g)",    1, "runehelmg.png"},
    {"rune platebody (t)",    1, "runebodyt.png"},
    {"rune platebody (g)",    1, "runebodyg.png"},
    {"rune platelegs (t)",    1, "runelegst.png"},
    {"rune platelegs (g)",    1, "runelegsg.png"},
    {"rune plateskirt (t)",   1, "runeskirtt.png"},
    {"rune plateskirt (g)",   1, "runeskirtg.png"},
    {"rune kiteshield (t)",   1, "runeshieldt.png"},
    {"rune kiteshield (g)",   1, "runeshieldg.png"},
    {"saradomin full helm",   1, "sarahelm.png"},
    {"saradomin platebody",   1, "sarabody.png"},
    {"saradomin platelegs",   1, "saralegs.png"},
    {"saradomin plateskirt",  1, "saraskirt.png"},
    {"saradomin kiteshield",  1, "sarashield.png"},
    {"zamorak full helm",     1, "zamhelm.png"},
    {"zamorak platebody",     1, "zambody.png"},
    {"zamorak platelegs",     1, "zamlegs.png"},
    {"zamorak plateskirt",    1, "zamskirt.png"},
    {"zamorak kiteshield",    1, "zamshield.png"},
    {"guthix full helm",      1, "guthhelm.png"},
    {"guthix platebody",      1, "guthbody.png"},
    {"guthix platelegs",      1, "guthlegs.png"},
    {"guthix plateskirt",     1, "guthskirt.png"},
    {"guthix kiteshield",     1, "guthshield.png"},
    {"blue d'hide body (t)",  1, "bluedbodyt.png"},
    {"blue d'hide body (g)",  1, "bluedbodyg.png"},
    {"blue d'hide chaps (t)", 1, "bluedchapst.png"},
    {"blue d'hide chaps (g)", 1, "bluedchapsg.png"},
    {"saradomin coif",        1, "saradcoif.png"},
    {"saradomin body",        1, "saradbody.png"},
    {"saradomin chaps",       1, "saradchaps.png"},
    {"saradomin vambraces",   1, "saradvamb.png"},
    {"zamorak coif",          1, "zamdcoif.png"},
    {"zamorak body",          1, "zamdbody.png"},
    {"zamorak chaps",         1, "zamdchaps.png"},
    {"zamorak vambraces",     1, "zamdvamb.png"},
    {"guthix coif",           1, "guthdcoif.png"},
    {"guthix body",           1, "guthdbody.png"},
    {"guthix chaps",          1, "guthdchaps.png"},
    {"guthix vambraces",      1, "guthdvamb.png"},
    {"enchanted hat",         1, "enchhat.png"},
    {"enchanted robe top",    1, "enchtop.png"},
    {"enchanted robe bottom", 1, "enchrobe.png"},
    {"saradomin stole",       1, "sarastole.png"},
    {"saradomin crozier",     1, "saracroz.png"},
    {"zamorak stole",         1, "zamstole.png"},
    {"zamorak crozier",       1, "zamcroz.png"},
    {"guthix stole",          1, "guthstole.png"},
    {"guthix crozier",        1, "guthcroz.png"},
    {"black cavalier",        1, "blackcava.png"},
    {"dark cavalier",         1, "darkcava.png"},
    {"tan cavalier",          1, "tancava.png"},
    {"robin hood hat",        1, "robin.png"},
    {"pirate's hat",          1, "pirate.png"},
    {"amulet of glory (t)",   1, "gloryt.png"},
    {SUPER_RARE_TABLE,        1},
}

local PAGE_TABLE = {
    {"saradomin page 1", 1, "sarapage.png"},
    {"saradomin page 2", 1, "sarapage.png"},
    {"saradomin page 3", 1, "sarapage.png"},
    {"saradomin page 4", 1, "sarapage.png"},
    {"zamorak page 1",   1, "zampage.png" },
    {"zamorak page 2",   1, "zampage.png" },
    {"zamorak page 3",   1, "zampage.png" },
    {"zamorak page 4",   1, "zampage.png" },
    {"guthix page 1",    1, "guthpage.png"},
    {"guthix page 2",    1, "guthpage.png"},
    {"guthix page 3",    1, "guthpage.png"},
    {"guthix page 4",    1, "guthpage.png"},
}

COMMON_REWARDS = {
    {"rune full helm",     1, "runehelm.png"},
    {"rune platebody",     1, "runebody.png"},
    {"rune platelegs",     1, "runelegs.png"},
    {"rune plateskirt",    1, "runeskirt.png"},
    {"black d'hide body",  1, "blackdbody.png"},
    {"black d'hide chaps", 1, "blackdchaps.png"},
    {"rune battleaxe",     1, "runebaxe.png"},
    {"rune axe",           1, "runeaxe.png"},
    {"rune pickaxe",       1, "runepick.png"},
    {"rune dagger",        1, "runedagger.png"},
    {"rune longsword",     1, "runelong.png"},
    {"magic shortbow",     1, "mageshort.png"},
    {"magic longbow",      1, "magelong.png"},
    {"magic comp bow",     1, "magecompbow.png"},
    {"lobster",           15, "lobster.png"},
    {"shark",             15, "shark.png"},
    {"purple sweets",     15, "sweets.png"},
    {"law rune",          30, "lawrune.png"},
    {"nature rune",       30, "natrune.png"},
    {"blood rune",        30, "bloodrune.png"},
    {"blue firelighter",  10, "bluefl.png"},
    {"red firelighter",   10, "redfl.png"},
    {"green firelighter", 10, "greenfl.png"},
    {"white firelighter", 10, "whitefl.png"},
    {"purple firelighter",10, "purplefl.png"},
    {"coins",           2500, "coins.png"},
    {PAGE_TABLE,           1},
}
