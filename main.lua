require("clue")

-- init graphics
local itemimg = {}
local rewardscreen
local casketimg
local rsfont
local rssfont

function reset_color ()
    love.graphics.setColor(255, 255, 255, 255)
end

function load_images (items)
    for _, item in pairs(items) do
        if type(item[1]) == "table" then
            load_images(item[1])
        elseif item[3] and type(item[3]) == "string" then
            itemimg[item[1]] = love.graphics.newImage("img/"..item[3])
        end
    end
end

function love.load ()
    load_images(RARE_REWARDS)
    load_images(COMMON_REWARDS)
    rewardscreen = love.graphics.newImage("img/rewscroll.png")
    casketimg = love.graphics.newImage("img/casket.png")
    rsfont = love.graphics.newFont("rs.ttf", 16)
    rssfont = love.graphics.newFont("rss.ttf", 16)
    love.graphics.setFont(rsfont)

    love.window.setMode(320, 350)
end

local reward = generate_reward()

function love.draw ()
    love.graphics.draw(rewardscreen, 0, 0)
    love.graphics.draw(casketimg, 50, 300)
    love.graphics.print("Open a casket", 90, 300)
    love.graphics.print("Caskets opened: "..tostring(get_num_clues()), 90, 0)

    -- draw the items
    local slot = 0
    local rewnames = {}
    for item, num in pairs(reward) do
        local x = 140 + (slot % 3) * 30
        local y = 80 + math.floor(slot / 3) * 30
        local itemx = x + 15 - math.floor(itemimg[item]:getWidth() / 2)
        local itemy = y + 15 - math.floor(itemimg[item]:getHeight() / 2)
        love.graphics.draw(itemimg[item], itemx, itemy)
        if (num > 1) then
            love.graphics.setFont(rssfont)
            -- text shadow
            love.graphics.setColor(0, 0, 0)
            love.graphics.print(tostring(num), x - 1, y - 1)
            love.graphics.setColor(255, 255, 0)
            love.graphics.print(tostring(num), x - 2, y - 2)
            love.graphics.setFont(rsfont)
            reset_color()
        end

        slot = slot + 1
        rewnames[slot] = item
    end

    -- show item name
    local mousex = love.mouse.getX()
    local mousey = love.mouse.getY()
    if mousex > 140 and mousex < 230 and mousey > 80 and mousey < 140 then
        local itemnumber = (math.floor((mousey - 80) / 30) * 3 + math.floor((mousex - 140) / 30)) + 1
        love.graphics.setColor(0, 0, 0)
        love.graphics.print(rewnames[itemnumber] or "", 30, 30)
        reset_color()
    end
end

function love.mousepressed (x, y)
    if y > 290 and y < 330 and x > 50 and x < 200 then
        reward = generate_reward()
    end
end
