cluegen - a RuneScape clue scroll reward generator
==================================================

This was a quick one-evening project. It uses the [LÖVE][love] framework.

There is really not much to say about this. It's a completely pointless, but
(possibly) fun program. If you've ever wanted to see bad loots without having
to do clues yourself, this is the program for you.

The easiest way to run this program to download the `.love` file [here][bbd]
and then run it with LÖVE which you can download from the link mentioned above.

[love]: http://love2d.org/ "LÖVE"
[bbd]: https://bitbucket.org/diyrunar/cluegen/downloads "Bitbucket: cluegen"
