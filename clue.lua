-- generates a random hard clue reward

-- initialize stuff
math.randomseed(os.time())
require("rewards")

local num_clues = 0
function get_num_clues ()
    return num_clues
end

local function get_reward_from_table (table)
    local reward = table[math.random(1, #table)]
    if type(reward[1]) == "table" then
        reward = get_reward_from_table(reward[1])
    end
    return reward
end

local function add_random_reward_to_list (list, rlist)
    local item = get_reward_from_table(rlist)
    local num
    if item[2] > 1 then
        num = math.floor(math.random(math.floor(item[2] * 0.75), math.floor(item[2] * 1.25)))
    else
        num = 1
    end
    list[item[1]] = (list[item[1]] or 0) + num
end

function generate_reward ()
    local reward = {}

    -- each hard clue has four chances of getting a rare reward
    for i = 1, 4 do
        if math.random(1, 20) == 20 then
            add_random_reward_to_list(reward, RARE_REWARDS)
        else
            add_random_reward_to_list(reward, COMMON_REWARDS)
        end
    end

    for i = 1, math.random(0, 2) do
        add_random_reward_to_list(reward, COMMON_REWARDS)
    end

    num_clues = num_clues + 1
    return reward
end
